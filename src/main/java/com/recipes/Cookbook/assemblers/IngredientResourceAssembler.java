package com.recipes.Cookbook.assemblers;

import com.recipes.Cookbook.assemblers.links.IngredientLinkHelper;
import com.recipes.Cookbook.entities.Ingredient;
import org.springframework.stereotype.Component;

@Component
public class IngredientResourceAssembler extends ItemAssembler<Ingredient> {

    public IngredientResourceAssembler(IngredientLinkHelper linkHelper) {
        super(linkHelper);
    }
}
