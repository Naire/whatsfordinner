package com.recipes.Cookbook.assemblers.links;

import com.recipes.Cookbook.exceptions.InsufficientInformationException;
import com.recipes.Cookbook.exceptions.ItemNotFoundException;
import com.recipes.Cookbook.exceptions.ItemNotUniqueException;
import java.util.Collection;
import org.springframework.hateoas.Link;

public interface LinkHelper<T, K> {

    Link getUpdateLink(K id, T item)
        throws ItemNotFoundException, ItemNotUniqueException, InsufficientInformationException;

    Link getAllLink(int pageNumber, int pageLimit, String sortBy, boolean isAsc);

    Link getAllLink();

    Link getUpdateLink(String name, T item)
        throws ItemNotFoundException, ItemNotUniqueException, InsufficientInformationException;

    Link getLinkToAddMany(Collection<T> items);

    Link getOneLink(K id) throws ItemNotFoundException;

    Link getFindByNameLink(String name) throws ItemNotFoundException;

}
