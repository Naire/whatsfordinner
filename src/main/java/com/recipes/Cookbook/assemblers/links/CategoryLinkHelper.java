package com.recipes.Cookbook.assemblers.links;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import com.recipes.Cookbook.controllers.CategoryController;
import com.recipes.Cookbook.entities.Category;
import com.recipes.Cookbook.exceptions.InsufficientInformationException;
import com.recipes.Cookbook.exceptions.ItemNotFoundException;
import com.recipes.Cookbook.exceptions.ItemNotUniqueException;
import java.util.Collection;
import org.springframework.hateoas.Link;
import org.springframework.stereotype.Component;

@Component
public class CategoryLinkHelper implements LinkHelper<Category, Long> {

    @Override
    public Link getUpdateLink(Long id, Category item)
        throws ItemNotFoundException, ItemNotUniqueException, InsufficientInformationException {
        return linkTo(methodOn(CategoryController.class).update(id, item)).withRel("update");
    }

    @Override
    public Link getAllLink(int pageNumber, int pageLimit, String sortBy, boolean isAsc) {
        return linkTo(
            methodOn(CategoryController.class).getAll(pageNumber, pageLimit, sortBy, isAsc))
            .withRel("all");
    }

    @Override
    public Link getAllLink() {
        return linkTo(
            methodOn(CategoryController.class).getAll())
            .withRel("all");
    }

    @Override
    public Link getUpdateLink(String name, Category item)
        throws ItemNotFoundException, ItemNotUniqueException, InsufficientInformationException {
        return linkTo(methodOn(CategoryController.class).update(name, item)).withRel("update");

    }

    @Override
    public Link getLinkToAddMany(Collection<Category> items) {
        return linkTo(methodOn(CategoryController.class).addMany(items)).withSelfRel();
    }

    @Override
    public Link getOneLink(Long id) throws ItemNotFoundException {
        return linkTo(methodOn(CategoryController.class).getOne(id)).withSelfRel();
    }

    @Override
    public Link getFindByNameLink(String name) throws ItemNotFoundException {
        return linkTo(methodOn(CategoryController.class).findByName(name)).withRel("name");
    }

}
