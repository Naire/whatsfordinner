package com.recipes.Cookbook.assemblers.links;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import com.recipes.Cookbook.controllers.RecipeController;
import com.recipes.Cookbook.entities.Recipe;
import com.recipes.Cookbook.exceptions.InsufficientInformationException;
import com.recipes.Cookbook.exceptions.ItemNotFoundException;
import com.recipes.Cookbook.exceptions.ItemNotUniqueException;
import org.springframework.hateoas.Link;
import org.springframework.stereotype.Component;

import java.util.Collection;

@Component
public class RecipeLinkHelper implements LinkHelper<Recipe, Long> {

    @Override
    public Link getUpdateLink(Long id, Recipe item)
        throws ItemNotFoundException, ItemNotUniqueException, InsufficientInformationException {
        return linkTo(methodOn(RecipeController.class).update(id, item)).withRel("update");
    }

    @Override
    public Link getAllLink(int pageNumber, int pageLimit, String sortBy, boolean isAsc) {
        return linkTo(
            methodOn(RecipeController.class).getAll(pageNumber, pageLimit, sortBy, isAsc))
            .withRel("all");
    }

    @Override
    public Link getAllLink() {
        return linkTo(
            methodOn(RecipeController.class).getAll())
            .withRel("all");
    }

    @Override
    public Link getUpdateLink(String name, Recipe item)
        throws ItemNotFoundException, ItemNotUniqueException, InsufficientInformationException {
        return linkTo(methodOn(RecipeController.class).update(name, item)).withRel("update");

    }

    @Override
    public Link getLinkToAddMany(Collection<Recipe> items) {
        return linkTo(methodOn(RecipeController.class).addMany(items)).withSelfRel();
    }

    @Override
    public Link getOneLink(Long id) throws ItemNotFoundException {
        return linkTo(methodOn(RecipeController.class).getOne(id)).withSelfRel();
    }

    @Override
    public Link getFindByNameLink(String name) throws ItemNotFoundException {
        return linkTo(methodOn(RecipeController.class).findByName(name)).withRel("name");
    }

    public Link getRecipeIngredientsLink(String ingredientIds) throws ItemNotFoundException {
        return linkTo(methodOn(RecipeController.class).getRecipesForIngredients(ingredientIds))
            .withRel("recipesForIngredients");
    }
}
