package com.recipes.Cookbook.assemblers.links;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import com.recipes.Cookbook.controllers.IngredientController;
import com.recipes.Cookbook.entities.Ingredient;
import com.recipes.Cookbook.exceptions.InsufficientInformationException;
import com.recipes.Cookbook.exceptions.ItemNotFoundException;
import com.recipes.Cookbook.exceptions.ItemNotUniqueException;
import java.util.Collection;
import org.springframework.hateoas.Link;
import org.springframework.stereotype.Component;

@Component
public class IngredientLinkHelper implements LinkHelper<Ingredient, Long> {

    @Override
    public Link getUpdateLink(Long id, Ingredient item)
        throws ItemNotFoundException, ItemNotUniqueException, InsufficientInformationException {
        return linkTo(methodOn(IngredientController.class).update(id, item)).withRel("update");
    }

    @Override
    public Link getAllLink(int pageNumber, int pageLimit, String sortBy, boolean isAsc) {
        return linkTo(
            methodOn(IngredientController.class).getAll(pageNumber, pageLimit, sortBy, isAsc))
            .withRel("all");
    }

    @Override
    public Link getAllLink() {
        return linkTo(
            methodOn(IngredientController.class).getAll())
            .withRel("all");
    }

    @Override
    public Link getUpdateLink(String name, Ingredient item)
        throws ItemNotFoundException, ItemNotUniqueException, InsufficientInformationException {
        return linkTo(methodOn(IngredientController.class).update(name, item)).withRel("update");

    }

    @Override
    public Link getLinkToAddMany(Collection<Ingredient> items) {
        return linkTo(methodOn(IngredientController.class).addMany(items)).withSelfRel();
    }

    @Override
    public Link getOneLink(Long id) throws ItemNotFoundException {
        return linkTo(methodOn(IngredientController.class).getOne(id)).withSelfRel();
    }

    @Override
    public Link getFindByNameLink(String name) throws ItemNotFoundException {
        return linkTo(methodOn(IngredientController.class).findByName(name)).withRel("name");
    }
}
