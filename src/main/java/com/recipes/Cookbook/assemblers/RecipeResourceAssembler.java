package com.recipes.Cookbook.assemblers;

import com.recipes.Cookbook.assemblers.links.RecipeLinkHelper;
import com.recipes.Cookbook.entities.Recipe;
import org.springframework.web.bind.annotation.ControllerAdvice;

@ControllerAdvice
public class RecipeResourceAssembler extends ItemAssembler<Recipe> {

    public RecipeResourceAssembler(RecipeLinkHelper linkHelper) {
        super(linkHelper);
    }
}
