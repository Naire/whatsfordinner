package com.recipes.Cookbook.assemblers;

import com.recipes.Cookbook.assemblers.links.LinkHelper;
import com.recipes.Cookbook.entities.Item;
import lombok.NoArgsConstructor;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.web.bind.annotation.ControllerAdvice;

@ControllerAdvice
@NoArgsConstructor
public class ItemAssembler<T extends Item> implements ResourceAssembler<T, Resource<T>> {

    private LinkHelper linkHelper;

    public ItemAssembler(LinkHelper linkHelper) {
        this.linkHelper = linkHelper;
    }

    @Override
    public Resource<T> toResource(T item) {
        try {
            return new Resource<>(item,
                linkHelper.getOneLink(item.getId()),
                linkHelper.getFindByNameLink(item.getName()),
                linkHelper.getAllLink(/*0, Integer.MAX_VALUE, "name", true*/)
            );
        } catch (Exception ex) {
            throw new RuntimeException("Unexpected error has occurred. " + ex.getMessage(), ex);
        }
    }
}
