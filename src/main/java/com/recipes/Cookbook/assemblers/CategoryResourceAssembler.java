package com.recipes.Cookbook.assemblers;

import com.recipes.Cookbook.assemblers.links.CategoryLinkHelper;
import com.recipes.Cookbook.entities.Category;
import org.springframework.stereotype.Component;

@Component
public class CategoryResourceAssembler extends ItemAssembler<Category> {

    public CategoryResourceAssembler(CategoryLinkHelper linkHelper) {
        super(linkHelper);
    }
}
