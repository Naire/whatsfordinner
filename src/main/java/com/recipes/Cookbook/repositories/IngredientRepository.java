package com.recipes.Cookbook.repositories;

import com.recipes.Cookbook.entities.Ingredient;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;
import java.util.Optional;

public interface IngredientRepository extends PagingAndSortingRepository<Ingredient, Long> {

    Optional<Ingredient> findByName(String name);

    List<Ingredient> findByCategoryId(Long id);
}
