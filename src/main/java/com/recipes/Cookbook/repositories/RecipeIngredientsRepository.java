package com.recipes.Cookbook.repositories;

import com.recipes.Cookbook.entities.RecipeIngredients;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface RecipeIngredientsRepository extends
    PagingAndSortingRepository<RecipeIngredients, Long> {

    void deleteByRecipeId(Long id);

}
