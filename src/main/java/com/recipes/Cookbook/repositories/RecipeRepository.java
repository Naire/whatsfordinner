package com.recipes.Cookbook.repositories;

import com.recipes.Cookbook.entities.Recipe;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface RecipeRepository extends PagingAndSortingRepository<Recipe, Long> {

    Optional<Recipe> findByName(String name);
}
