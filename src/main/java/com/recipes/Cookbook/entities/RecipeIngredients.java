package com.recipes.Cookbook.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import org.springframework.lang.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Data
@Entity
public class RecipeIngredients {

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    @Nullable
    @JoinColumn
    private Recipe recipe;

    @Transient
    @Nullable
    private String recipeName;

    @ManyToOne
    @JoinColumn
    private Ingredient ingredient;

    @Transient
    @Nullable
    private String ingredientName;

    @NotNull
    @Column(nullable = false)
    private Long quantity; //todo should be double? floating point

    @NotNull
    @Column(nullable = false)
    private String quantityUnit;

    public String getRecipeName() {
        return recipe == null ? recipeName : recipe.getName();
    }

    @JsonIgnore
    public void setRecipe(Recipe recipe) {
        this.recipe = recipe;
    }

    public String getIngredientName() {
        return ingredient == null ? ingredientName : ingredient.getName();
    }

    @JsonIgnore
    public void setIngredient(Ingredient ingredient) {
        this.ingredient = ingredient;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RecipeIngredients that = (RecipeIngredients) o;
        return getId().equals(that.getId()) &&
            Objects.equals(getRecipe(), that.getRecipe()) &&
            Objects.equals(getRecipeName(), that.getRecipeName()) &&
            Objects.equals(getIngredient(), that.getIngredient()) &&
            Objects.equals(getIngredientName(), that.getIngredientName()) &&
            Objects.equals(getQuantity(), that.getQuantity()) &&
            Objects.equals(getQuantityUnit(), that.getQuantityUnit());
    }

    @Override
    public int hashCode() {
        return Objects
            .hash(getId(), getRecipe(), getRecipeName(), getIngredient(), getIngredientName(),
                getQuantity(), getQuantityUnit());
    }

    @Override
    public String toString() {
        String sb = "RecipeIngredients{" + "id=" + id
            + ", recipe=" + recipe
            + ", recipeName='" + recipeName + '\''
            + ", ingredient=" + ingredient
            + ", ingredientName='" + ingredientName + '\''
            + ", quantity=" + quantity
            + ", quantityUnit='" + quantityUnit + '\''
            + '}';
        return sb;
    }
}
