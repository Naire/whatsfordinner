package com.recipes.Cookbook.entities;

import lombok.Data;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Data
@Entity
public class Recipe implements Item<Long> {

    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    @Column(unique = true, nullable = false)
    private String name;

    @NotNull
    @Column(columnDefinition = "TEXT", nullable = false)
    private String description;

    @NotNull
    @Column(nullable = false)
    private String picture;

    @NotNull
    @OneToMany(mappedBy = "recipe", orphanRemoval = true, cascade = CascadeType.REMOVE)
    private Set<RecipeIngredients> recipeIngredients;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Recipe recipe = (Recipe) o;
        return Objects.equals(getId(), recipe.getId()) &&
            Objects.equals(getName(), recipe.getName()) &&
            Objects.equals(getPicture(), recipe.getPicture().trim()) &&
            Objects.equals(getDescription(), recipe.getDescription());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getDescription(), getPicture());
    }

    @Transient
    @Override
    public Recipe createCopy() {
        final Recipe result = new Recipe();
        result.setId(getId());
        result.setName(getName());
        result.setDescription(getDescription());
        result.setPicture(getPicture() == null ? "" : getPicture().trim());
        final Set<RecipeIngredients> recipeIngredients = getRecipeIngredients();
        result.setRecipeIngredients(
            recipeIngredients == null ? new HashSet<>() : new HashSet<>(recipeIngredients));
        return result;
    }

}
