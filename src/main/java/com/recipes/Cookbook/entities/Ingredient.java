package com.recipes.Cookbook.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import org.springframework.lang.Nullable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import java.util.Objects;
import java.util.Set;

@Data
@Entity
public class Ingredient implements Item<Long> {

    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    @Column(unique = true, nullable = false)
    private String name;

    @ManyToOne
    @Nullable
    @JoinColumn(name = "category_id")
    private Category category;

    @Transient
    @NotNull
    private String categoryName;

    @Nullable
    @JsonIgnore
    @OneToMany(mappedBy = "ingredient", fetch = FetchType.LAZY, orphanRemoval = true, cascade = CascadeType.REMOVE)
    private Set<RecipeIngredients> recipeIngredients;


    @Nullable
    private Boolean isVegetarian;
    @Nullable
    private Boolean isGlutenFree;
    @Nullable
    private Boolean isLactoseFree;


    public String getCategoryName() {
        return category == null ? categoryName : category.getName();
    }

    @JsonIgnore
    public void setCategory(Category category) {
        this.category = category;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Ingredient that = (Ingredient) o;
        return Objects.equals(getId(), that.getId()) &&
            Objects.equals(getName(), that.getName()) &&
            Objects.equals(getCategoryName(), that.getCategoryName()) &&
            Objects.equals(getIsVegetarian(), that.getIsVegetarian()) &&
            Objects.equals(getIsGlutenFree(), that.getIsGlutenFree()) &&
            Objects.equals(getIsLactoseFree(), that.getIsLactoseFree());
    }

    @Override
    public int hashCode() {
        return Objects
            .hash(getId(), getName(), getCategoryName(), getIsVegetarian(), getIsGlutenFree(),
                getIsLactoseFree());
    }

    @Transient
    @Override
    public Ingredient createCopy() {
        final Ingredient result = new Ingredient();
        result.setId(getId());
        result.setName(getName());
        result.setCategory(getCategory());
        result.setCategoryName(getCategoryName());
        result.setIsGlutenFree(getIsGlutenFree());
        result.setIsLactoseFree(getIsLactoseFree());
        result.setIsVegetarian(getIsVegetarian());
        result.setRecipeIngredients(getRecipeIngredients());
        return result;
    }

    @Override
    public String toString() {
        return "Ingredient{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", categoryName='" + getCategoryName() + '\'' +
            ", isVegetarian=" + isVegetarian +
            ", isGlutenFree=" + isGlutenFree +
            ", isLactoseFree=" + isLactoseFree +
            '}';
    }
}
