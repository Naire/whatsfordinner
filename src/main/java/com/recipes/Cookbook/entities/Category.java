package com.recipes.Cookbook.entities;


import javax.persistence.FetchType;
import lombok.Data;
import org.springframework.lang.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Objects;

@Data
@Entity
public class Category implements Item<Long> {

    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    @Column(unique = true, nullable = false)
    private String name;

    @Nullable
    @OneToMany(mappedBy = "category", fetch = FetchType.LAZY)
    private List<Ingredient> ingredients;

    @Transient
    @Override
    public Category createCopy() {
        final Category result = new Category();
        result.setId(getId());
        result.setName(getName());
        result.setIngredients(getIngredients());
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Category category = (Category) o;
        return Objects.equals(getId(), category.getId()) &&
            Objects.equals(getName(), category.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName());
    }
}
