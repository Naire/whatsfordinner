package com.recipes.Cookbook.entities;

public interface Item<K> {

    K getId();

    void setId(K id);

    String getName();

    void setName(String name);

    Item<K> createCopy();
}
