package com.recipes.Cookbook.services;

import com.recipes.Cookbook.config.TransactionalRollbackCheckedEx;
import com.recipes.Cookbook.dto.IngredientDTO;
import com.recipes.Cookbook.dto.RecipeDTO;
import com.recipes.Cookbook.entities.Recipe;
import com.recipes.Cookbook.exceptions.InsufficientInformationException;
import com.recipes.Cookbook.exceptions.ItemNotFoundException;
import com.recipes.Cookbook.exceptions.ItemNotUniqueException;
import com.recipes.Cookbook.repositories.RecipeIngredientsRepository;
import com.recipes.Cookbook.repositories.RecipeRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Collection;

@Slf4j
@Service
@TransactionalRollbackCheckedEx
public class RecipeService extends ItemService<Recipe, Long> {

    @PersistenceContext
    private EntityManager entityManager;
    private RecipeRepository recipeRepository;
    private RecipeIngredientsRepository recipeIngredientsRepository;
    private RecipeDTO dto;
    private IngredientDTO ingredientDTO;

    {
        ITEM_NAME = "Recipe";
    }

    public RecipeService(RecipeRepository recipeRepository,
                         RecipeIngredientsRepository recipeIngredientsRepository, RecipeDTO dto,
                         IngredientDTO ingredientDTO) {
        super(recipeRepository);
        this.recipeRepository = recipeRepository;
        this.recipeIngredientsRepository = recipeIngredientsRepository;
        this.dto = dto;
        this.ingredientDTO = ingredientDTO;
    }

    @Override
    public Recipe save(final Recipe item) throws ItemNotUniqueException, ItemNotFoundException {
        final Recipe save = super.save(item);
        final Recipe recipeForDB = dto.getRecipeForDB(save);
        recipeIngredientsRepository.saveAll(recipeForDB.getRecipeIngredients());
        return recipeForDB;
    }

    @Override
    public Recipe findByName(final String name) throws ItemNotFoundException {
        return recipeRepository.findByName(name)
                .orElseThrow(() -> new ItemNotFoundException(ITEM_NAME, UNIQUE_FIELD, name));
    }

    @Override
    protected void deleteDependenciesBeforeUpdate(Long id) {
        recipeIngredientsRepository.deleteByRecipeId(id);
    }

    @Override
    protected void insertUpdatedRowsAfterDeletion(Recipe itemForUpdate)
            throws InsufficientInformationException, ItemNotFoundException {
        recipeIngredientsRepository
                .saveAll(ingredientDTO.getRecipeIngredientsForDB(itemForUpdate, "update"));
    }

    @Override
    protected void updateNotNullFields(Recipe toBeUpdated, Recipe copiedFrom) {
        if (copiedFrom.getName() != null) {
            toBeUpdated.setName(copiedFrom.getName());
        }
        if (copiedFrom.getDescription() != null) {
            toBeUpdated.setDescription(copiedFrom.getDescription());
        }
        if (copiedFrom.getRecipeIngredients() != null) {
            toBeUpdated.setRecipeIngredients(copiedFrom.getRecipeIngredients());
        }
    }

    /**
     * Gets recipes which have at least one ingredient with id given in the ids parameter.
     *
     * @param ids a list of ids of ingredients in format 3,4,14,5
     * @return collection of Recipe
     */
    public Collection<Recipe> getRecipesForIngredients(String ids) {
        Query query = entityManager
                .createNativeQuery(
                        "select * from Recipe where id in "
                                + "(select distinct recipe_id from Recipe_Ingredients "
                                + "where recipe_id not in ("
                                + "select distinct recipe_id from ("
                                + "select distinct recipe_id, case when ingredient_id in (" + ids
                                + ") then 'ok' else 'no' end as status "
                                + "from Recipe_Ingredients) where status = 'no'))", Recipe.class);
        return (Collection<Recipe>) query.getResultList();
    }
}
