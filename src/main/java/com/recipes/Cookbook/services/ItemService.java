package com.recipes.Cookbook.services;

import com.recipes.Cookbook.config.TransactionalRollbackCheckedEx;
import com.recipes.Cookbook.entities.Item;
import com.recipes.Cookbook.exceptions.InsufficientInformationException;
import com.recipes.Cookbook.exceptions.ItemNotFoundException;
import com.recipes.Cookbook.exceptions.ItemNotUniqueException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.ArrayList;
import java.util.Collection;

@Slf4j
@TransactionalRollbackCheckedEx
public abstract class ItemService<T extends Item<K>, K> {

    protected String ITEM_NAME = "Item";
    protected String UNIQUE_FIELD = "name";
    PagingAndSortingRepository<T, K> repository;

    public ItemService(PagingAndSortingRepository<T, K> repository) {
        this.repository = repository;
    }

    public T findOne(final K id) throws ItemNotFoundException {
        return repository.findById(id).orElseThrow(() ->
            new ItemNotFoundException(ITEM_NAME, "id", String.valueOf(id)));
    }

    public abstract T findByName(final String name) throws ItemNotFoundException;

    public Page<T> findAll(PageRequest pageRequest) {
        return repository.findAll(pageRequest);
    }

    public Iterable<T> findAll() {
        return repository.findAll(orderByName());
    }

    private Sort orderByName() {
        return new Sort(Sort.Direction.ASC, "name");
    }

    public T save(final T item) throws ItemNotUniqueException, ItemNotFoundException {
        try {
            findByName(item.getName());
            throw new ItemNotUniqueException(ITEM_NAME, UNIQUE_FIELD, item.getName());
        } catch (ItemNotFoundException e) {
            //if not found then it can be created
        }
        return repository.save(item);
    }


    //todo: inform user which items weren't saved
    public Collection<T> saveAll(final Collection<T> items) {
        final Collection<T> saved = new ArrayList<>(items.size());
        for (T item : items) {
            try {
                saved.add(save(item));
            } catch (Exception ex) {
                log.error(String.format("An exception has occurred during saving item: %s", item),
                    ex);
            }
        }
        return saved;
    }

    public T updateName(final String itemName, final String newName)
        throws ItemNotFoundException, ItemNotUniqueException {
        nameIsUniqueOrThrowException(newName);
        final T item = findByName(itemName);
        item.setName(newName);
        return repository.save(item);
    }

    public void delete(K id) throws ItemNotFoundException {
        repository.delete(findOne(id));
    }

    public void deleteByName(String name) throws ItemNotFoundException {
        repository.delete(findByName(name));
    }

    protected void nameIsUniqueOrThrowException(String newName) throws ItemNotUniqueException {
        try {
            findByName(newName);
            throw new ItemNotUniqueException(ITEM_NAME, UNIQUE_FIELD, newName);
        } catch (ItemNotFoundException ex) {
            //name is unique, can be changed
        }
    }

    /**
     * Update completely overwrites if specified - for example if only one ingredient is provided,
     * all old ingredients will be removed and this one specified will be persisted instead. If the
     * field is not provided at all, then old values are left intact.
     * <p>
     * This is a template method, methods: deleteDependenciesBeforeUpdate,
     * insertUpdatedRowsAfterDeletion and updateNotNullFields are configurable steps which should be
     * implemented accordingly. Only updateNotNullFields is mandatory to override.
     * <p>
     * Steps: 1. Finds old item by id in the db. 2. If new item is null returns old item. 3.
     * prepareForUpdate method. 4. Creates copy of old item. 5. updateNotNullFields 6. Save item to
     * repository 7. Set id which was returned after saving item in db 8.
     * deleteDependenciesBeforeUpdate 9. insertUpdatedRowsAfterDeletion
     */
    public T update(K id, T newItem)
        throws ItemNotFoundException, ItemNotUniqueException, InsufficientInformationException {
        final T oldItem = findOne(id);
        if (newItem == null) {
            return oldItem;
        }

        return updateItem(newItem, oldItem);
    }

    public T update(String name, T newItem)
        throws ItemNotFoundException, ItemNotUniqueException, InsufficientInformationException {
        final T oldItem = findByName(name);
        if (newItem == null) {
            return oldItem;
        }
        return updateItem(newItem, oldItem);
    }


    /**
     * Step of update: if names are not equal checks if the new name is unique in the db.
     */
    protected void prepareForUpdate(T newItem, T oldItem)
        throws ItemNotUniqueException {
        if (!oldItem.getName().equals(newItem.getName())) {
            nameIsUniqueOrThrowException(newItem.getName());
        }
    }

    /**
     * Step of update: copies fields from copiedFrom into toBeUpdated if they are not null.
     */
    protected abstract void updateNotNullFields(T toBeUpdated, T copiedFrom)
        throws ItemNotFoundException;

    /**
     * Step of update: if there are any dependent entities, appropriate rows needs to be deleted
     * before new, updated ones are inserted. By default does nothing, should be overridden if
     * necessary.
     *
     * @param id of item which will be updated, should be a foreign key of the table from which rows
     * should be deleted.
     */
    protected void deleteDependenciesBeforeUpdate(K id) {
    }

    /**
     * Step of update: if there were any dependent entities and old ones have been removed, this
     * method should insert new, updated rows. By default this method does nothing, should be
     * overridden if necessary.
     *
     * @param itemForUpdate item which undergoes update
     */
    protected void insertUpdatedRowsAfterDeletion(T itemForUpdate)
        throws ItemNotFoundException, InsufficientInformationException {

    }

    private T updateItem(T newItem, T oldItem)
        throws ItemNotUniqueException, ItemNotFoundException, InsufficientInformationException {
        prepareForUpdate(newItem, oldItem);
        final T itemForUpdate = (T) oldItem.createCopy();
        updateNotNullFields(itemForUpdate, newItem);
        final T save = repository.save(itemForUpdate);
        itemForUpdate.setId(save.getId());
        deleteDependenciesBeforeUpdate((K) save.getId());
        insertUpdatedRowsAfterDeletion(itemForUpdate);
        return itemForUpdate;
    }
}
