package com.recipes.Cookbook.services;

import com.recipes.Cookbook.entities.Category;
import com.recipes.Cookbook.exceptions.ItemNotFoundException;
import com.recipes.Cookbook.repositories.CategoryRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class CategoryService extends ItemService<Category, Long> {

    private CategoryRepository categoryRepository;

    {
        ITEM_NAME = "Category";
    }

    public CategoryService(CategoryRepository categoryRepository) {
        super(categoryRepository);
        this.categoryRepository = categoryRepository;
    }

    @Override
    public Category findByName(final String name) throws ItemNotFoundException {
        return categoryRepository.findByName(name).orElseThrow(() ->
            new ItemNotFoundException(ITEM_NAME, UNIQUE_FIELD, name));
    }

    @Override
    protected void updateNotNullFields(Category toBeUpdated, Category copiedFrom) {
        if (copiedFrom.getName() != null) {
            toBeUpdated.setName(copiedFrom.getName());
        }
    }
}
