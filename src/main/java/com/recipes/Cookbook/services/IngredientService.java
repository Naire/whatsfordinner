package com.recipes.Cookbook.services;

import com.recipes.Cookbook.entities.Category;
import com.recipes.Cookbook.entities.Ingredient;
import com.recipes.Cookbook.exceptions.InsufficientInformationException;
import com.recipes.Cookbook.exceptions.ItemNotFoundException;
import com.recipes.Cookbook.exceptions.ItemNotUniqueException;
import com.recipes.Cookbook.repositories.CategoryRepository;
import com.recipes.Cookbook.repositories.IngredientRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class IngredientService extends ItemService<Ingredient, Long> {

    protected IngredientRepository ingredientRepository;
    protected CategoryRepository categoryRepository;

    {
        ITEM_NAME = "Ingredient";
    }

    public IngredientService(IngredientRepository ingredientRepository,
        CategoryRepository categoryRepository) {
        super(ingredientRepository);
        this.ingredientRepository = ingredientRepository;
        this.categoryRepository = categoryRepository;
    }

    public Ingredient findByName(final String name) throws ItemNotFoundException {
        return ingredientRepository.findByName(name).orElseThrow(() ->
            new ItemNotFoundException(ITEM_NAME, UNIQUE_FIELD, name));
    }

    /**
     * If category name is specified but not present in the system then the ingredient is NOT added
     */
    @Override
    public Ingredient save(final Ingredient item)
            throws ItemNotUniqueException, ItemNotFoundException {
        final String categoryName = item.getCategoryName();
        item.setCategory(categoryRepository.findByName(categoryName)
                .orElseThrow(() -> new ItemNotFoundException("Category", UNIQUE_FIELD, categoryName)));
        return super.save(item);
    }

    @Override
    protected void updateNotNullFields(Ingredient toBeUpdated, Ingredient copiedFrom)
        throws ItemNotFoundException {
        if (copiedFrom.getName() != null) {
            toBeUpdated.setName(copiedFrom.getName());
        }
        final String categoryName = copiedFrom.getCategoryName();
        if (categoryName != null) {
            toBeUpdated.setCategory(findCategoryByName(categoryName));
        }
        if (copiedFrom.getIsLactoseFree() != null) {
            toBeUpdated.setIsLactoseFree(copiedFrom.getIsLactoseFree());
        }
        if (copiedFrom.getIsVegetarian() != null) {
            toBeUpdated.setIsVegetarian(copiedFrom.getIsVegetarian());
        }
        if (copiedFrom.getIsGlutenFree() != null) {
            toBeUpdated.setIsGlutenFree(copiedFrom.getIsGlutenFree());
        }
    }


    public Ingredient updateCategoryForIngredient(final String ingredientName,
        final String categoryName)
        throws ItemNotFoundException {
        final Ingredient ingredient = findByName(ingredientName);
        if (ingredient.getCategory() != null && ingredient.getCategory().getName()
            .equals(categoryName)) {
            return ingredient;
        }
        final Category category = findCategoryByName(categoryName);
        ingredient.setCategory(category);
        return ingredientRepository.save(ingredient);
    }

    public List<Ingredient> findIngredientsByNames(final Collection<String> ingredientNames)
        throws ItemNotFoundException {
        try {
            return ingredientNames.stream()
                .map(ingredientRepository::findByName)
                .map(Optional::get)
                .collect(Collectors.toList());
        } catch (NoSuchElementException e) {
            throw new ItemNotFoundException(
                "Some of the ingredients are not available in the system. Aborting.");
        }
    }

    public Ingredient findIngredientByName(final String name) throws ItemNotFoundException {
        try {
            return ingredientRepository.findByName(name).get();
        } catch (NoSuchElementException e) {
            throw new ItemNotFoundException("Ingredient", UNIQUE_FIELD, name);
        }
    }

    public List<Ingredient> findByCategoryId(final long categoryId) throws ItemNotFoundException {
        try{
          return ingredientRepository.findByCategoryId(categoryId);
        } catch (NoSuchElementException e) {
            throw new ItemNotFoundException(
                "Cannot find ingredients by given category.");
        }
    }

    private Category findCategoryByName(final String categoryName) throws ItemNotFoundException {
        return categoryRepository.findByName(categoryName)
            .orElseThrow(() -> new ItemNotFoundException("Category", UNIQUE_FIELD, categoryName));
    }

}
