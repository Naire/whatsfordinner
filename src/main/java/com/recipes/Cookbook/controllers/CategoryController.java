package com.recipes.Cookbook.controllers;

import com.recipes.Cookbook.assemblers.ItemAssembler;
import com.recipes.Cookbook.assemblers.links.CategoryLinkHelper;
import com.recipes.Cookbook.entities.Category;
import com.recipes.Cookbook.exceptions.InsufficientInformationException;
import com.recipes.Cookbook.exceptions.ItemNotFoundException;
import com.recipes.Cookbook.exceptions.ItemNotUniqueException;
import com.recipes.Cookbook.services.CategoryService;
import java.util.Collection;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping(value = "/categories",
    produces = MediaType.APPLICATION_JSON_VALUE)
public class CategoryController extends ItemController<Category, Long> {


    public CategoryController(ItemAssembler<Category> assembler, CategoryService service,
        CategoryLinkHelper linkHelper) {
        super(assembler, service, linkHelper);
    }

    //todo: only admin should be allowed to make any changes to categories
    //add index on categories name
    //only admin should have access to ID

    @PostMapping
    public ResponseEntity<Resource<Category>> addOne(@RequestBody Category category)
            throws ItemNotUniqueException, ItemNotFoundException {
        return super.addOne(category);
    }

    @PostMapping("/batch")
    public ResponseEntity<Resources<?>> addMany(@RequestBody Collection<Category> categories) {
        return super.addMany(categories);
    }

    @GetMapping
    public Resources<Resource<Category>> getAll(
        @RequestParam(value = "page", required = false, defaultValue = "0") int pageNumber,
        @RequestParam(value = "limit", required = false, defaultValue = "10") int pageLimit,
        @RequestParam(value = "sortBy", required = false, defaultValue = "name") String sortBy,
        @RequestParam(value = "isAsc", required = false, defaultValue = "true") boolean isAsc) {
        return super.getAll(pageNumber, pageLimit, sortBy, isAsc);
    }

    @GetMapping("/all")
    public Resources<Resource<Category>> getAll() {
        return super.getAll();
    }


    @GetMapping("/{id}")
    public Resource<Category> getOne(@PathVariable long id) throws ItemNotFoundException {
        return super.getOne(id);
    }

    @GetMapping("/{name}")
    public Resource<Category> findByName(@PathVariable String name) throws ItemNotFoundException {
        return super.findByName(name);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Resource<Category>> update(@PathVariable Long id,
        @RequestBody Category category)
        throws ItemNotFoundException, ItemNotUniqueException, InsufficientInformationException {
        return super.update(id, category);
    }

    @PutMapping("/name/{name}")
    public ResponseEntity<Resource<Category>> update(@PathVariable String name,
        @RequestBody Category category)
        throws ItemNotFoundException, ItemNotUniqueException, InsufficientInformationException {
        return super.update(name, category);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable Long id) throws ItemNotFoundException {
        return super.delete(id);
    }

    @DeleteMapping("/name/{name}")
    public ResponseEntity delete(@PathVariable String name) throws ItemNotFoundException {
        return super.delete(name);
    }
}
