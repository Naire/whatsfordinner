package com.recipes.Cookbook.controllers;

import com.recipes.Cookbook.assemblers.RecipeResourceAssembler;
import com.recipes.Cookbook.assemblers.links.RecipeLinkHelper;
import com.recipes.Cookbook.entities.Ingredient;
import com.recipes.Cookbook.entities.Recipe;
import com.recipes.Cookbook.exceptions.InsufficientInformationException;
import com.recipes.Cookbook.exceptions.ItemNotFoundException;
import com.recipes.Cookbook.exceptions.ItemNotUniqueException;
import com.recipes.Cookbook.services.RecipeService;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping(value = "/recipes",
    produces = MediaType.APPLICATION_JSON_VALUE)
public class RecipeController extends ItemController<Recipe, Long> {

    private RecipeService service;
    private RecipeResourceAssembler assembler;
    private RecipeLinkHelper linkHelper;

    public RecipeController(RecipeService service, RecipeResourceAssembler assembler,
        RecipeLinkHelper linkHelper) {
        super(assembler, service, linkHelper);
        this.service = service;
        this.assembler = assembler;
        this.linkHelper = linkHelper;
    }

    @PostMapping
    public ResponseEntity<Resource<Recipe>> addOne(@RequestBody Recipe recipe)
            throws ItemNotUniqueException, ItemNotFoundException {
        return super.addOne(recipe);
    }

    @PostMapping("/batch")
    public ResponseEntity<Resources<?>> addMany(
        @RequestBody Collection<Recipe> recipes) {
        return super.addMany(recipes);
    }

    @GetMapping
    public Resources<Resource<Recipe>> getAll(
        @RequestParam(value = "page", required = false, defaultValue = "0") int pageNumber,
        @RequestParam(value = "limit", required = false, defaultValue = "10") int pageLimit,
        @RequestParam(value = "sortBy", required = false, defaultValue = "name") String sortBy,
        @RequestParam(value = "isAsc", required = false, defaultValue = "true") boolean isAsc) {
        return super.getAll(pageNumber, pageLimit, sortBy, isAsc);
    }

    @GetMapping("/all")
    public Resources<Resource<Recipe>> getAll() {
        return super.getAll();
    }

    @GetMapping("/{id}")
    public Resource<Recipe> getOne(@PathVariable Long id) throws ItemNotFoundException {
        return super.getOne(id);
    }

    @GetMapping("/name/{name}")
    public Resource<Recipe> findByName(@PathVariable String name) throws ItemNotFoundException {
        return super.findByName(name);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Resource<Recipe>> update(@PathVariable Long id,
        @RequestBody Recipe recipe)
        throws ItemNotFoundException, ItemNotUniqueException, InsufficientInformationException {
        return super.update(id, recipe);
    }

    @PutMapping("/name/{name}")
    public ResponseEntity<Resource<Recipe>> update(@PathVariable String name,
        @RequestBody Recipe recipe)
        throws ItemNotFoundException, ItemNotUniqueException, InsufficientInformationException {
        return super.update(name, recipe);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable Long id) throws ItemNotFoundException {
        return super.delete(id);
    }

    @DeleteMapping("/name/{name}")
    public ResponseEntity delete(@PathVariable String name) throws ItemNotFoundException {
        return super.delete(name);
    }

    @GetMapping("/ingredients/{ids}")
    public Resources<Resource<Recipe>> getRecipesForIngredients(@PathVariable String ids) throws ItemNotFoundException {
        return new Resources<>(
            service.getRecipesForIngredients(ids).stream()
                .map(assembler::toResource)
                .collect(Collectors.toList()),
            linkHelper.getRecipeIngredientsLink(ids));
    }

}
