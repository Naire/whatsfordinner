package com.recipes.Cookbook.controllers;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import com.recipes.Cookbook.assemblers.IngredientResourceAssembler;
import com.recipes.Cookbook.assemblers.links.IngredientLinkHelper;
import com.recipes.Cookbook.entities.Ingredient;
import com.recipes.Cookbook.exceptions.InsufficientInformationException;
import com.recipes.Cookbook.exceptions.ItemNotFoundException;
import com.recipes.Cookbook.exceptions.ItemNotUniqueException;
import com.recipes.Cookbook.services.IngredientService;
import com.recipes.Cookbook.services.ItemService;
import com.recipes.Cookbook.util.ControllerHelper;

import java.util.Collection;
import java.util.stream.Collectors;

import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.hateoas.Resources;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping(value = "/ingredients",
    produces = MediaType.APPLICATION_JSON_VALUE)
public class IngredientController extends ItemController<Ingredient, Long> {

    private IngredientService service;
    private ResourceAssembler<Ingredient, Resource<Ingredient>> assembler;

    public IngredientController(IngredientResourceAssembler assembler, IngredientService service,
        IngredientLinkHelper linkHelper) {
        super(assembler, service, linkHelper);
        this.service = service;
        this.assembler = assembler;
    }

    @PostMapping
    public ResponseEntity<Resource<Ingredient>> addOne(@RequestBody Ingredient ingredient)
            throws ItemNotUniqueException, ItemNotFoundException {
        return super.addOne(ingredient);
    }

    @PostMapping("/batch")
    public ResponseEntity<Resources<?>> addMany(
        @RequestBody Collection<Ingredient> items) {
        return super.addMany(items);
    }

    @GetMapping
    public Resources<Resource<Ingredient>> getAll(
        @RequestParam(value = "page", required = false, defaultValue = "0") int pageNumber,
        @RequestParam(value = "limit", required = false, defaultValue = "10") int pageLimit,
        @RequestParam(value = "sortBy", required = false, defaultValue = "name") String sortBy,
        @RequestParam(value = "isAsc", required = false, defaultValue = "true") boolean isAsc) {
        return super.getAll(pageNumber, pageLimit, sortBy, isAsc);
    }

    @GetMapping("/all")
    public Resources<Resource<Ingredient>> getAll() {
        return super.getAll();
    }

    @GetMapping("/{id}")
    public Resource<Ingredient> getOne(@PathVariable Long id) throws ItemNotFoundException {
        return super.getOne(id);
    }

    @GetMapping("/name/{name}")
    public Resource<Ingredient> findByName(@PathVariable String name)
        throws ItemNotFoundException {
        return super.findByName(name);
    }

    @GetMapping("/category/{id}")
    public Resources<Resource<Ingredient>> findByCategory(@PathVariable Long id)
        throws ItemNotFoundException {
        return new Resources<>(
            service.findByCategoryId(id)
                .stream()
                .map(assembler::toResource)
                .collect(Collectors.toList()),
            linkTo(methodOn(IngredientController.class).findByCategory(id)).withSelfRel());
    }

    @PutMapping("/{id}")
    public ResponseEntity<Resource<Ingredient>> update(@PathVariable Long id,
        @RequestBody Ingredient item)
        throws ItemNotFoundException, ItemNotUniqueException, InsufficientInformationException {
        return super.update(id, item);
    }

    @PutMapping("/name/{name}")
    public ResponseEntity<Resource<Ingredient>> update(@PathVariable String name,
        @RequestBody Ingredient item)
        throws ItemNotFoundException, ItemNotUniqueException, InsufficientInformationException {
        return super.update(name, item);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable Long id) throws ItemNotFoundException {
        return super.delete(id);
    }

    @DeleteMapping("/name/{name}")
    public ResponseEntity delete(@PathVariable String name) throws ItemNotFoundException {
        return super.delete(name);
    }
}
