package com.recipes.Cookbook.controllers;

import com.recipes.Cookbook.assemblers.ItemAssembler;
import com.recipes.Cookbook.assemblers.links.LinkHelper;
import com.recipes.Cookbook.entities.Item;
import com.recipes.Cookbook.exceptions.InsufficientInformationException;
import com.recipes.Cookbook.exceptions.ItemNotFoundException;
import com.recipes.Cookbook.exceptions.ItemNotUniqueException;
import com.recipes.Cookbook.services.ItemService;
import com.recipes.Cookbook.util.ControllerHelper;
import java.net.URI;
import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.hateoas.Resources;
import org.springframework.http.ResponseEntity;

public class ItemController<T extends Item<K>, K> {

    private ItemService<T, K> service;
    private ResourceAssembler<T, Resource<T>> assembler;
    private LinkHelper<T, K> linkHelper;

    public ItemController(ItemAssembler<T> assembler, ItemService<T, K> service,
        LinkHelper<T, K> linkHelper) {
        this.service = service;
        this.assembler = assembler;
        this.linkHelper = linkHelper;
    }
    public Resources<Resource<T>> getAll(int pageNumber, int pageLimit, String sortBy,
        boolean isAsc) {
        return new Resources<>(
            service.findAll(ControllerHelper.getPageRequest(pageNumber, pageLimit, sortBy, isAsc))
                .stream()
                .map(assembler::toResource)
                .collect(Collectors.toList()),
            linkHelper.getAllLink(pageNumber, pageLimit, sortBy, isAsc));
    }

    public Resources<Resource<T>> getAll() {
        return new Resources<>(
            StreamSupport.stream(service.findAll().spliterator(), false)
                .map(assembler::toResource)
                .collect(Collectors.toList()),
            linkHelper.getAllLink());
    }


    public ResponseEntity<Resource<T>> addOne(T item)
            throws ItemNotUniqueException, ItemNotFoundException {
        final Resource<T> resource = assembler.toResource(service.save(item));
        return ResponseEntity.created(URI.create(resource.getId().expand().getHref()))
            .body(resource);
    }

    public ResponseEntity<Resources<?>> addMany(Collection<T> items) {
        Resources<Resource<T>> resources = new Resources<>(
            service.saveAll(items).stream()
                .map(assembler::toResource)
                .collect(Collectors.toList()),
            linkHelper.getLinkToAddMany(items));
        return ResponseEntity.created(URI.create(resources.getId().expand().getHref()))
            .body(resources);
    }

    public Resource<T> getOne(K id) throws ItemNotFoundException {
        return assembler.toResource(service.findOne(id));
    }

    public Resource<T> findByName(String name) throws ItemNotFoundException {
        return assembler.toResource(service.findByName(name));
    }

    public ResponseEntity<Resource<T>> update(K id, T item)
        throws ItemNotFoundException, ItemNotUniqueException, InsufficientInformationException {
        final Resource<T> resource = assembler
            .toResource(service.update(id, item));
        resource.add(linkHelper.getUpdateLink(id, item));
        return ResponseEntity.ok(resource);
    }

    public ResponseEntity<Resource<T>> update(String name, T item)
        throws ItemNotFoundException, ItemNotUniqueException, InsufficientInformationException {
        final Resource<T> resource = assembler
            .toResource(service.update(name, item));
        resource.add(linkHelper.getUpdateLink(name, item));
        return ResponseEntity.ok(resource);
    }

    public ResponseEntity delete(K id) throws ItemNotFoundException {
        service.delete(id);
        return ResponseEntity.noContent().build();
    }

    public ResponseEntity delete(String name) throws ItemNotFoundException {
        service.deleteByName(name);
        return ResponseEntity.noContent().build();
    }
}
