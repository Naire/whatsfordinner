package com.recipes.Cookbook.util;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

public class ControllerHelper {

    public static PageRequest getPageRequest(int pageNumber, int pageLimit, String sortBy,
        boolean isAsc) {
        Sort sort = Sort.by(sortBy);
        sort = isAsc ? sort.ascending() : sort.descending();
        return PageRequest.of(pageNumber, pageLimit, sort);
    }
}
