package com.recipes.Cookbook.exceptions;

import org.springframework.http.HttpStatus;

public abstract class ExceptionWithHttpStatus extends Exception {

    private static final String DEFAULT_MSG = "Unknown error has occurred.";
    private HttpStatus httpStatusCode = HttpStatus.INTERNAL_SERVER_ERROR;

    public ExceptionWithHttpStatus() {
        super(DEFAULT_MSG);
    }

    public ExceptionWithHttpStatus(final String message) {
        super(message);
    }

    public HttpStatus getHttpStatusCode() {
        return httpStatusCode;
    }

    protected void setHttpStatusCode(final HttpStatus statusCode) {
        this.httpStatusCode = statusCode;
    }


}
