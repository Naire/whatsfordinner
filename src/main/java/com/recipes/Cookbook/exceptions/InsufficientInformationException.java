package com.recipes.Cookbook.exceptions;

import org.springframework.http.HttpStatus;

public class InsufficientInformationException extends ExceptionWithHttpStatus {


    private static final String
        DEFAULT_MSG =
        "Not enough information about the item to perform this operation.";
    private static final String
        FORMATTED_MSG =
        "Not enough information about the item: %s to perform operation: %s.";

    public InsufficientInformationException() {
        this(DEFAULT_MSG);
    }

    public InsufficientInformationException(String whichItem, String whichOperation) {
        this(String.format(FORMATTED_MSG, whichItem, whichOperation));
    }

    public InsufficientInformationException(String message) {
        super(message);
        setHttpStatusCode(HttpStatus.BAD_REQUEST);
    }

}
