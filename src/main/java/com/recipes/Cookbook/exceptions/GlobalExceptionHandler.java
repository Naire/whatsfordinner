package com.recipes.Cookbook.exceptions;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.internal.engine.ConstraintViolationImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler({ExceptionWithHttpStatus.class, TransactionSystemException.class})
    public final ResponseEntity<String> handleException(Exception ex) {
        if (ex instanceof ExceptionWithHttpStatus) {
            log.error("Exception with HttpStatus has occurred.", ex);
            return new ResponseEntity<>(ex.getMessage(),
                    ((ExceptionWithHttpStatus) ex).getHttpStatusCode());
        }

        if (ex instanceof TransactionSystemException) {
            Throwable rootCause = ((TransactionSystemException) ex).getRootCause();
            log.error("TransactionSystemException has occurred. Root cause: ", rootCause);
            String message = ex.getMessage();
            if (rootCause instanceof ConstraintViolationException) {
                Set<ConstraintViolation<?>> constraintViolations =
                        ((ConstraintViolationException) rootCause).getConstraintViolations();
                message = constraintViolations.stream()
                        .filter(violation -> violation instanceof ConstraintViolationImpl)
                        .map(constraintViolation ->
                                String.format("%s %s for %s, ",
                                        constraintViolation.getPropertyPath(),
                                        constraintViolation.getMessage(),
                                        constraintViolation.getRootBeanClass().getSimpleName())
                        )
                        .collect(Collectors.joining());
                message = message.replaceAll(", $", "");
            }
            return new ResponseEntity<>(message, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        log.error("An unknown error has occurred.", ex);
        return new ResponseEntity<>(
                "Unknown error has occurred. Exception message: " + ex.getMessage(),
                HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
