package com.recipes.Cookbook.exceptions;

import org.springframework.http.HttpStatus;

public class ItemNotFoundException extends ExceptionWithHttpStatus {

    private static final String DEFAULT_MSG = "Item has not been found.";
    private static final String FORMATTED_MSG = "%s with %s: %s has not been found.";

    public ItemNotFoundException() {
        this(DEFAULT_MSG);
    }

    public ItemNotFoundException(String whichItem, String whichField, String fieldValue) {
        this(String.format(FORMATTED_MSG, whichItem, whichField, fieldValue));
    }

    public ItemNotFoundException(String message) {
        super(message);
        setHttpStatusCode(HttpStatus.NOT_FOUND);
    }
}
