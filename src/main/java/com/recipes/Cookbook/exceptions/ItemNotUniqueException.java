package com.recipes.Cookbook.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;

public class ItemNotUniqueException extends ExceptionWithHttpStatus {

    private static final String
        DEFAULT_MSG =
        "Item with provided name is already present in the system. Name has to be unique.";
    private static final String
        FORMATTED_MSG =
        "%s with provided %s: %s is already present in the system. %s has to be unique.";

    public ItemNotUniqueException() {
        this(DEFAULT_MSG);
    }

    public ItemNotUniqueException(String whichItem, String whichField, String fieldValue) {
        this(String.format(FORMATTED_MSG, whichItem, whichField, fieldValue,
            StringUtils.capitalize(whichField)));
    }

    public ItemNotUniqueException(String message) {
        super(message);
        setHttpStatusCode(HttpStatus.CONFLICT);
    }
}
