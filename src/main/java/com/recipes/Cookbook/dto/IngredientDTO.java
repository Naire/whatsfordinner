package com.recipes.Cookbook.dto;


import com.recipes.Cookbook.entities.Ingredient;
import com.recipes.Cookbook.entities.Recipe;
import com.recipes.Cookbook.entities.RecipeIngredients;
import com.recipes.Cookbook.exceptions.InsufficientInformationException;
import com.recipes.Cookbook.exceptions.ItemNotFoundException;
import com.recipes.Cookbook.services.IngredientService;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
public class IngredientDTO {

  private IngredientService service;

  public IngredientDTO(IngredientService service) {
    this.service = service;
  }

  public Set<RecipeIngredients> getRecipeIngredientsForDB(Recipe recipe, String operation)
      throws ItemNotFoundException, InsufficientInformationException {
    Set<RecipeIngredients> set = new HashSet<>();
    for (RecipeIngredients item : recipe.getRecipeIngredients()) {
      System.out.println(item);
      if (item.getIngredientName() == null || item.getQuantity() == null
          || item.getQuantityUnit() == null) {
        throw new InsufficientInformationException("recipe ingredient", operation);
      }
      final Ingredient ingredient = service.findIngredientByName(item.getIngredientName());
      RecipeIngredients newRecipeIngredient = new RecipeIngredients();
      newRecipeIngredient.setIngredient(ingredient);
      newRecipeIngredient.setQuantity(item.getQuantity());
      newRecipeIngredient.setQuantityUnit(item.getQuantityUnit());
      newRecipeIngredient.setRecipe(recipe);
      set.add(newRecipeIngredient);
    }
    return set;
  }
}
