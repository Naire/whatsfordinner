package com.recipes.Cookbook.dto;

import com.recipes.Cookbook.entities.Ingredient;
import com.recipes.Cookbook.entities.Recipe;
import com.recipes.Cookbook.entities.RecipeIngredients;
import com.recipes.Cookbook.exceptions.ItemNotFoundException;
import com.recipes.Cookbook.repositories.IngredientRepository;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class RecipeDTO {

    private IngredientRepository ingredientRepository;

    public RecipeDTO(IngredientRepository ingredientRepository) {
        this.ingredientRepository = ingredientRepository;
    }

    public Recipe getRecipeForDB(Recipe recipeFromJson) throws ItemNotFoundException {
        final Recipe result = recipeFromJson.createCopy();
        final List<IngredientQuantityPair> ingredients = getIngredientQuantityPairs(
            result.getRecipeIngredients());
        result.setRecipeIngredients(getRecipeIngredientsSet(ingredients, result));
        return result;
    }

    private Set<RecipeIngredients> getRecipeIngredientsSet(
        List<IngredientQuantityPair> ingredientQuantityPairs, Recipe result) {
        return ingredientQuantityPairs.stream()
            .map(pair -> {
                final RecipeIngredients recipeIngredients = new RecipeIngredients();
                recipeIngredients.setIngredient(pair.ingredient);
                recipeIngredients.setRecipe(result);
                recipeIngredients.setQuantity(pair.quantity);
                recipeIngredients.setQuantityUnit(pair.unit);
                return recipeIngredients;
            })
            .collect(Collectors.toSet());
    }

    private List<IngredientQuantityPair> getIngredientQuantityPairs(
        Set<RecipeIngredients> recipeIngredients)
        throws ItemNotFoundException {
        if (recipeIngredients == null) {
            throw new ItemNotFoundException(
                "Ingredients are required to create a recipe. Aborting.");
        }
        final List<IngredientQuantityPair> newPairs = new ArrayList<>();
        try {
            final IngredientQuantityPair pair = new IngredientQuantityPair();
            recipeIngredients.stream()
                .map(recipeIngredient -> {
                    pair.quantity = recipeIngredient.getQuantity();
                    pair.unit = recipeIngredient.getQuantityUnit();
                    return recipeIngredient.getIngredientName();
                })
                .map(ingredientRepository::findByName)
                .forEach(ingredient -> {
                    //streams are going vertically - quantities will match ingredients
                    pair.ingredient = ingredient.get();
                    newPairs.add(new IngredientQuantityPair(pair));
                });
        } catch (NoSuchElementException ex) {
            throw new ItemNotFoundException(
                "All ingredients are required to create a recipe. "
                    + "Some of the provided ingredients were not found in the database. "
                    + "Aborting.");
        }
        return newPairs;
    }

    @NoArgsConstructor
    private class IngredientQuantityPair {

        Ingredient ingredient;
        Long quantity;
        String unit;

        IngredientQuantityPair(IngredientQuantityPair pair) {
            this.ingredient = pair.ingredient.createCopy();
            this.quantity = new Long(pair.quantity);
            this.unit = pair.unit;
        }

        //todo: remove all tostrings

        @Override
        public String toString() {
            return "IngredientQuantityPair{" +
                "ingredient=" + ingredient.getName() +
                ", quantity=" + quantity +
                ", unit=" + unit +
                '}';
        }
    }
}
